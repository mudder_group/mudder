package com.project.mudder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.player_action.view.*

class PlayerActionRecyclerAdapter(private val actionList : ArrayList<Play.PlayerAction>): RecyclerView.Adapter<PlayerActionRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.player_action, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(actionList[position])
    }

    override fun getItemCount(): Int {
        return actionList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindItems(action : Play.PlayerAction){
            val textViewTitle = itemView.userActionTitle as TextView
            val textViewDesc = itemView.userActionDesc as TextView
            textViewTitle.text = action.actionName
            textViewDesc.text = action.actionDesc
        }
    }

    fun addItem(action : Play.PlayerAction){
        actionList.add(action)
        notifyDataSetChanged()
    }
}