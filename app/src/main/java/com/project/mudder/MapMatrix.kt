package com.project.mudder

import java.util.*

@Suppress("SENSELESS_COMPARISON", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MapMatrix(firstRoom: Models.Room) {
    var matrix: Array<Array<Models.Room>>
    var id: Int = 1

    init {
        matrix = arrayOf(arrayOf(firstRoom))
        firstRoom.id = id
        id++
    }

    private fun getRoomPosition(): Pair<Int, Int> {
        for ((countRow, row) in matrix.withIndex()){
            if(row != null){
                for ((countColumn, value) in row.withIndex()){
                    if(value != null){
                        if(value.isPlayerInRoom)
                            return Pair(countRow, countColumn)
                    }
                }
            }
        }
        return Pair(-1, -1)
    }

    fun getCurrentRoom(): Models.Room {
        val (row, column) = getRoomPosition()
        return matrix[row][column]
    }

    private fun shiftMatrix(array:Array<Array<Models.Room>?>): Array<Array<Models.Room>?> {
        val list = array.toList()
        Collections.rotate(list, 1)
        return list.toTypedArray()
    }

    private fun shiftArray(array: Array<Models.Room>?): Array<Models.Room>? {
        val list = array?.toList()
        Collections.rotate(list, 1)
        return list?.toTypedArray()
    }

    @Suppress("UNCHECKED_CAST")
    private fun resizeMatrix(direction: Char){
        lateinit var result: Array<Array<Models.Room>?>
        when(direction){
            'N' -> {
                result = matrix.copyOf(matrix.size + 1)
                result = shiftMatrix(result)
                val tempArray = arrayOfNulls<Models.Room>(matrix[0].size)
                result[0] = tempArray as Array<Models.Room>
            }
            'S' -> {
                result = matrix.copyOf(matrix.size + 1)
                val tempArray = arrayOfNulls<Models.Room>(matrix[0].size)
                result[result.size - 1] = tempArray as Array<Models.Room>
            }
            'W' -> {
                result = matrix as Array<Array<Models.Room>?>
                val newSize = matrix[0].size + 1
                for(rowIndex in matrix.indices){
                    result[rowIndex] = resizeArray(newSize, matrix[rowIndex]) as Array<Models.Room>
                    result[rowIndex] = shiftArray(result[rowIndex])
                }
            }
            'E' -> {
                result = matrix as Array<Array<Models.Room>?>
                val newSize = matrix[0].size + 1
                for(rowIndex in matrix.indices){
                    result[rowIndex] = resizeArray(newSize, matrix[rowIndex]) as Array<Models.Room>
                }
            }
        }
        matrix = result as Array<Array<Models.Room>>
    }

    private fun resizeArray(newSize: Int, array: Array<Models.Room>): Array<Models.Room?> {
        val newArray = arrayOfNulls<Models.Room>(newSize)
        for(i in 0 until newSize - 1){
            newArray[i] = array.getOrNull(i)
        }
        return newArray
    }

    private fun updateMatrix(currentRow: Int, currentColumn: Int, newRow: Int, newColumn: Int, newRoom: Models.Room){
        if(matrix[newRow][newColumn] != null){
            matrix[newRow][newColumn].isPlayerInRoom = true
            matrix[newRow][newColumn].description = newRoom.description
            matrix[newRow][newColumn].entities = newRoom.entities
            matrix[currentRow][currentColumn].isPlayerInRoom = false
        }
        else{
            matrix[newRow][newColumn] = newRoom
            newRoom.id = id
            id++
        }
    }

    private fun updateMatrixObjects(row: Int, column: Int, direction: Char, newRoom: Models.Room){
        matrix[row][column].isPlayerInRoom = false
        newRoom.isPlayerInRoom = true
        when(direction){
            'N' -> updateMatrix(row, column, row - 1, column, newRoom)
            'E' -> updateMatrix(row, column, row, column + 1, newRoom)
            'S' -> updateMatrix(row, column, row + 1, column, newRoom)
            'W' -> updateMatrix(row, column, row, column - 1, newRoom)
        }
    }

    fun reallocMatrix(direction: Char, newRoom: Models.Room){
        val (row, column) = getRoomPosition()
        var isResized = false
        if(
                column == matrix[0].size - 1 && direction == 'E'
                || row == matrix.size - 1 && direction == 'S'
                || column == 0 && direction == 'W'
                || row == 0 && direction == 'N'
        ){
            resizeMatrix(direction)
            isResized = true
        }
        if(isResized){
            val (newRow, newColumn) = getRoomPosition()
            updateMatrixObjects(newRow, newColumn, direction, newRoom)
        }
        else{
            updateMatrixObjects(row, column, direction, newRoom)
        }
    }
}