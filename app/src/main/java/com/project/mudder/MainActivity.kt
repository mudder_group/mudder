package com.project.mudder

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_player_name.view.*

class MainActivity : AppCompatActivity() {

    private var url = "http://10.0.2.2:5000"

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initTitleTextView()

        play_btn.setOnClickListener {

            val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_player_name, null)
            val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)//.setTitle("Pseudo")
            val alertDialog = dialogBuilder.show()
            dialogView.dialogConfirm_btn.setOnClickListener{
                alertDialog.dismiss()
                val username = dialogView.username_textview.text.toString()
                val intent = Intent(this, Play::class.java)
                intent.putExtra("username", username)
                startActivity(intent)
            }

            dialogView.dialogCancel_btn.setOnClickListener {
                alertDialog.dismiss()
            }
        }
    }

    private fun initTitleTextView(){
        val requestQueue = Volley.newRequestQueue(this)
        val objectRequest = StringRequest(
            Request.Method.GET,
            url,
            {
                response ->
                Title_textview.text = response
            },
            {
                Title_textview.text = getString(R.string.connexion_error)
            }
        )
        requestQueue.add(objectRequest)
    }
}