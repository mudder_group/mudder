package com.project.mudder

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_play.*
import kotlinx.android.synthetic.main.alert_text.*
import kotlinx.android.synthetic.main.alert_text.view.*
import kotlinx.android.synthetic.main.dialog_extended_room.view.*
import kotlinx.android.synthetic.main.dialog_extended_room.view.dialogExtendedCancel_btn
import kotlinx.android.synthetic.main.dialog_extended_room.view.dialog_entities_list
import kotlinx.android.synthetic.main.dungeon_map.view.*
import kotlinx.android.synthetic.main.dungeon_map_room.view.*
import org.json.JSONObject
import kotlin.collections.ArrayList
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory as KotlinJsonAdapterFactory


class Play : AppCompatActivity(), View.OnClickListener {

    private lateinit var  recyclerView : RecyclerView
    private lateinit var adapterRecyclerView : PlayerActionRecyclerAdapter
    private lateinit var  player: Models.Player
    private lateinit var dungeonMap: MapMatrix

    private var baseUrl = "http://10.0.2.2:5000"
    data class PlayerAction(val actionName: String, val actionDesc: String)

    override fun onCreate(savedInstanceBundle: Bundle?){
        super.onCreate(savedInstanceBundle)
        setContentView(R.layout.activity_play)

        val username = intent.extras?.getString("username").toString()
        username_TextView.text = getString(R.string.quest_title, username)

        setButtonsOnClickListeners()

        //RecyclerView handling
        val recyclerView = actionsPlay as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        val actions = ArrayList<PlayerAction>()
        val adapter = PlayerActionRecyclerAdapter(actions)
        recyclerView.adapter = adapter

        this.recyclerView = recyclerView
        this.adapterRecyclerView = adapter

        connectToServer(username)

    }

    override fun onClick(p0: View?) {
        when(p0){
            NorthArrow_btn -> move("N")
            EastArrow_btn -> move("E")
            SouthArrow_btn -> move("S")
            WestArrow_btn -> move("W")

            View_btn -> {
                viewEntities()
            }

            Attack_btn -> {
                attackEntities()
            }

            Map_btn -> {
                setupMatrix()
            }
        }
    }

    private inline fun <reified T: Any> moshiHandler(response: String): T {
        val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
        val elementAdapter = moshi.adapter(T::class.java)
        return elementAdapter.fromJson(response) ?: throw Exception("Unable to convert json response to element class")
    }

    private fun setButtonsOnClickListeners(){
        NorthArrow_btn.setOnClickListener(this)
        EastArrow_btn.setOnClickListener(this)
        WestArrow_btn.setOnClickListener(this)
        SouthArrow_btn.setOnClickListener(this)
        Map_btn.setOnClickListener(this)
        View_btn.setOnClickListener(this)
        Attack_btn.setOnClickListener(this)
    }

    private fun handleDirectionButtons(playerInstance: Models.Player){
        val buttonsList = listOf<ImageButton>(EastArrow_btn, NorthArrow_btn, SouthArrow_btn, WestArrow_btn)
        buttonsList.forEach {
            enableButton(it, false)
        }
        playerInstance.room.paths.forEach {
            when(it){
                'E' -> enableButton(EastArrow_btn, true)
                'N' -> enableButton(NorthArrow_btn, true)
                'S' -> enableButton(SouthArrow_btn, true)
                'W' -> enableButton(WestArrow_btn, true)
            }
        }
    }

    private fun enableButton(button: ImageButton, boolean: Boolean){
        if(boolean){
            button.visibility = View.VISIBLE
        }else{
            button.visibility = View.INVISIBLE
        }
    }

    private fun setupMatrix(){
        val dungeonMapView = LayoutInflater.from(this).inflate(R.layout.dungeon_map, null)
        val dungeonMapViewBuilder = AlertDialog.Builder(this).setView(dungeonMapView)
        val table = dungeonMapView.table_map as TableLayout

        val mapDialog = dungeonMapViewBuilder.show()
        for (i in dungeonMap.matrix){
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            for (j in i){
                val mapRoom = LayoutInflater.from(this).inflate(R.layout.dungeon_map_room, null)
                @Suppress("SENSELESS_COMPARISON")
                if (j == null)
                    mapRoom.visibility = View.INVISIBLE
                else{
                    mapRoom.card_map_btn.apply {
                        layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
                        text = j.id.toString()
                        if(j.isPlayerInRoom){
                            setBackgroundColor(Color.YELLOW)
                            setTextColor(Color.BLACK)
                        }
                        j.paths.forEach {
                            when(it){
                                'N' -> mapRoom.upArrowMap_imgBtn.visibility = View.VISIBLE
                                'S' -> mapRoom.downArrowMap_imgBtn.visibility = View.VISIBLE
                                'E' -> mapRoom.rightArrowMap_imgBtn.visibility = View.VISIBLE
                                'W' -> mapRoom.leftArrowMap_imgBtn.visibility = View.VISIBLE
                            }
                        }
                        card_map_btn.setOnClickListener {
                            alertText("Observation", j.description)
                        }
                    }
                }
                row.addView(mapRoom)
            }
            table.addView(row)
        }

        dungeonMapView.quitMap_btn.setOnClickListener {
            mapDialog.dismiss()
        }
    }

    private fun handleError(error: VolleyError){
        if(error.networkResponse.statusCode == 404){
            throwError("Impossible de trouver l'entité recherchée.")
            return
        }
        try{
            val responseBody = String(error.networkResponse.data, Charsets.UTF_8)
            val responseError = moshiHandler<Models.Error>(responseBody)

            adapterRecyclerView.addItem(PlayerAction(responseError.type, responseError.message))
            (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(recyclerView.adapter!!.itemCount - 1)

            alertText(responseError.type, responseError.message)
        }
        catch (e: Exception){
            throwError("Une erreur de connexion est survenue. Veuillez vérifier votre connexion Internet.")
        }
    }

    private fun throwError(message: String){
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(message)
                .setCancelable(false)
                .setNeutralButton("OK") { dialog, _ ->
                    dialog.cancel()
                    finish()
                }
        val alert = alertDialogBuilder.create()
        alert.setTitle("Attention !")
        alert.show()
    }

    private fun alertText(type: String, message: String) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.alert_text, null)
        val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)
        val alertDialog = dialogBuilder.show()

        alertDialog.alertText_title.text = type
        alertDialog.alertTextView.text = message

        dialogView.alertTextAccept_btn.setOnClickListener {
            alertDialog.dismiss()
            if(type == "Mort")
                finish()
        }
    }

    //Server actions

    private fun connectToServer(username: String){
        val url = "$baseUrl/connect"
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(
            Method.POST,
            url,
            Response.Listener
            {
                response ->
                player = moshiHandler(response)
                dungeonMap = MapMatrix(player.room)
                health_TextView.text = getString(R.string.player_health, player.health, player.totalHealth)

                adapterRecyclerView.addItem(PlayerAction("Entrée dans la première pièce du donjon !", player.room.description))
                (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(recyclerView.adapter!!.itemCount - 1)

                handleDirectionButtons(player)
                Attack_btn.visibility = View.VISIBLE
                View_btn.visibility = View.VISIBLE
                Map_btn.visibility = View.VISIBLE
            },
            Response.ErrorListener
            {
                error ->
                handleError(error)
            }
        )
        {
            override fun getBodyContentType(): String{
                return "application/json"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray{
                val params: MutableMap<String, String> = HashMap()
                params["Username"] = username
                return JSONObject(params as Map<*, *>).toString().toByteArray()
            }
        }
        requestQueue.add(stringRequest)
    }

    private fun viewEntities(){
        val url = "$baseUrl/${player.guid}/noms"
        val requestQueue = Volley.newRequestQueue(this)
        val objectRequest = StringRequest(
            Request.Method.GET,
            url,
            {
                    response ->
                val entities = moshiHandler<Models.EntitiesLists>(response)

                val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_extended_room, null)
                val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)

                val linearLayout = dialogView.dialog_entities_list as LinearLayout
                val alertDialog = dialogBuilder.show()

                if (entities.guidsList.isEmpty()){
                    val textView = TextView(this)
                    textView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    textView.text = getString(R.string.empty_room_text)
                    linearLayout.addView(textView)
                }
                else{
                    entities.namesList.forEachIndexed { index, s ->
                        val button = Button(this)
                        val guid = entities.guidsList[index]
                        button.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        button.text = s

                        button.setOnClickListener{
                            viewEntity(guid)
                            alertDialog.dismiss()
                        }
                        linearLayout.addView(button)
                    }
                }

                dialogView.dialogViewSelectRoom_btn.setOnClickListener {
                    viewDungeonRoom()
                    alertDialog.dismiss()
                }

                dialogView.dialogViewSelectPlayer_btn.setOnClickListener {
                    viewEntity(player.guid)
                    alertDialog.dismiss()
                }

                dialogView.dialogExtendedCancel_btn.setOnClickListener {
                    alertDialog.dismiss()
                }
            },
            {
                    error ->
                handleError(error)
            }
        )
        requestQueue.add(objectRequest)
    }

    private fun viewEntity(guid: String){
        val url = "$baseUrl/${player.guid}/examiner/$guid"
        val requestQueue = Volley.newRequestQueue(this)
        val objectRequest = StringRequest(
            Request.Method.GET,
            url,
            {
                response ->
                val watch = moshiHandler<Models.Examination>(response)
                val message = watch.description + " avec une santé de " + watch.health + "/" + watch.totalHealth + "."

                if(guid == player.guid)
                    health_TextView.text = getString(R.string.player_health, watch.health, player.totalHealth)

                adapterRecyclerView.addItem(PlayerAction("Examination !", message))
                (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(recyclerView.adapter!!.itemCount - 1)
            },
            {
                error ->
                handleError(error)
            }
        )
        requestQueue.add(objectRequest)
    }

    private fun attackEntities(){
        val url = "$baseUrl/${player.guid}/noms"
        val requestQueue = Volley.newRequestQueue(this)
        val objectRequest = StringRequest(
            Request.Method.GET,
            url,
            {
                response ->
                val entities = moshiHandler<Models.EntitiesLists>(response)

                val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_extended_entities, null)
                val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)

                val linearLayout = dialogView.dialog_entities_list as LinearLayout
                val alertDialog = dialogBuilder.show()

                if (entities.guidsList.isEmpty()){
                    val textView = TextView(this)
                    textView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    textView.text = getString(R.string.empty_room_text)
                    linearLayout.addView(textView)
                }
                else{
                    entities.namesList.forEachIndexed { index, s ->
                        val button = Button(this)
                        val guid = entities.guidsList[index]
                        button.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        button.text = s

                        button.setOnClickListener{
                            attackEntity(guid)
                            alertDialog.dismiss()
                        }
                        linearLayout.addView(button)
                    }
                }

                dialogView.dialogExtendedCancel_btn.setOnClickListener {
                    alertDialog.dismiss()
                }

            },
            {
                error ->
                handleError(error)
            }
        )
        requestQueue.add(objectRequest)
    }

    private fun attackEntity(target: String){
        val url = "$baseUrl/${player.guid}/taper"
        val requestQueue = Volley.newRequestQueue(this)
        val objectRequest: StringRequest = object: StringRequest(
            Method.POST,
            url,
            Response.Listener
            {
                response ->
                val attack = moshiHandler<Models.Attack>(response)

                val resultText = "Vous avez attaqué ${attack.receiver.guid} " +
                        "en lui infligeant ${attack.sender.damage} points de dégâts. Il lui reste ${attack.receiver.health} points de vie." +
                        " Celui-ci a contre-attaqué en vous infligeant ${attack.receiver.damage} points de dégâts." +
                        " Il ne vous reste que ${attack.sender.health} points de vie."
                health_TextView.text = getString(R.string.player_health, attack.sender.health, player.totalHealth)

                adapterRecyclerView.addItem(PlayerAction("Attaque !", resultText))
                (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(recyclerView.adapter!!.itemCount - 1)
            },
            Response.ErrorListener
            {
                    error ->
                handleError(error)
            }
        )
        {
            override fun getBodyContentType(): String{
                return "application/json"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray{
                val params: MutableMap<String, String> = HashMap()
                params["cible"] = target
                return JSONObject(params as Map<*, *>).toString().toByteArray()
            }
        }
        requestQueue.add(objectRequest)
    }

    private fun viewDungeonRoom(){
        val url = "$baseUrl/${player.guid}/regarder"
        val requestQueue = Volley.newRequestQueue(this)
        val objectRequest = StringRequest(
                Request.Method.GET,
                url,
                {
                    response ->
                    val room = moshiHandler<Models.Room>(response)
                    adapterRecyclerView.addItem(PlayerAction("Observation", room.description))
                    (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(recyclerView.adapter!!.itemCount - 1)

                    val mapRoom = dungeonMap.getCurrentRoom()
                    mapRoom.description = room.description
                    mapRoom.entities = room.entities
                },
                {
                    error ->
                    handleError(error)
                }
        )
        requestQueue.add(objectRequest)
    }

    private fun move(direction : String){
        val url = "$baseUrl/${player.guid}/deplacement"
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                url,
                Response.Listener
                {
                    response ->
                    val room = moshiHandler<Models.Room>(response)
                    dungeonMap.reallocMatrix(direction.first(), room)

                    adapterRecyclerView.addItem(PlayerAction("Entrée dans une pièce !", room.description))
                    (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(recyclerView.adapter!!.itemCount - 1)
                    player.room = room
                    handleDirectionButtons(player)
                },
                Response.ErrorListener
                {
                    error ->
                    handleError(error)
                }
        )
        {
            override fun getBodyContentType(): String{
                return "application/json"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray{
                val params: MutableMap<String, String> = HashMap()
                params["direction"] = direction
                return JSONObject(params as Map<*, *>).toString().toByteArray()
            }
        }
        requestQueue.add(stringRequest)
    }
}