package com.project.mudder

import com.squareup.moshi.FromJson
import com.squareup.moshi.Json
import kotlin.properties.Delegates

class Models {

    class Player(
        val guid: String,
        @Json(name = "vie") var health: Int,
        @Json(name = "totalvie") val totalHealth: Int,
        @Json(name = "salle") var room : Room
    )

    class Room (
            var description: String,
            @Json(name = "passages") val paths: List<Char>,
            @Json(name = "entites") var entities: List<String>
    ){
        var id by Delegates.notNull<Int>()
        //If a room is created, it means that the player entered it, thus having isPlayerInRoom as true by default
        var isPlayerInRoom: Boolean = true
    }

    class Examination(
        val description: String,
        val type: String,
        @Json(name = "vie") val health: Int,
        @Json(name = "totalvie") val totalHealth: Int
    )

    class Attack(
        @Json(name="attaquant") var sender: AttackEntity,
        @Json(name="attaque") var receiver: AttackEntity
    )

    class AttackEntity(
        val guid: String,
        @Json(name="degats") val damage: Int,
        @Json(name="vie") val health: Int
    )

    class Error(
        val type: String,
        val message: String
    )

    class EntitiesLists(
            @Json(name="listenoms") val namesList: List<String>,
            @Json(name="listeguids") val guidsList: List<String>
    )
}